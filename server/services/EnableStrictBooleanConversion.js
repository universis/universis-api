import {ApplicationService, LangUtils} from '@themost/common';
import { FunctionContext } from '@themost/data';
import { DataModel } from '@themost/data';

const parseBoolean = LangUtils.parseBoolean;

/**
 * @this FunctionContext
 * @param {*} target 
 * @param {string} property 
 * @returns Promise<boolean>
 */
function convertStrictBoolean(target, property) {
    if (Object.prototype.hasOwnProperty.call(target, property)) {
        return Promise.resolve(parseBoolean(target[property]))
    }
    const primaryKey = this.model.getPrimaryKey();
    if (Object.prototype.hasOwnProperty.call(target, primaryKey)) {
        const key = target[primaryKey]
        return this.model.context.model(this.model.name)
            .where(primaryKey).equal(key)
            .select(property).value().then((value) => {
                return Promise.resolve(parseBoolean(value));
        });
    }
    return Promise.resolve(false);
}

class EnableStrictBooleanConversion extends ApplicationService {
    /**
     * @param {import('@themost/express').ExpressDataApplication} app
     */
    constructor(app) {
        super(app);
        if (typeof FunctionContext.prototype.convertStrictBoolean === 'function') {
            
            Object.assign(FunctionContext.prototype, {
                convertStrictBoolean
            })
        }

        const superCast = DataModel.prototype.cast;
        /**
         * Overrides DataModel.cast() method and converts int to boolean for attributes of type Boolean
         * @this DataModel
         */
        function cast(objectOrArray, state) {

            if (Array.isArray(objectOrArray)) {
                return objectOrArray.map((object) => {
                    return this.cast(object, state);
                });
            }
            const result = superCast.call(this, objectOrArray, state);
            if (result == null) {
                return result;
            }           
            const attributes = this.attributes;
            Object.keys(result).forEach((key) => {
                if (Object.prototype.hasOwnProperty.call(result, key)) {
                    const value = result[key];
                    if (value != null) {
                        const found = attributes.findIndex((x) => x.name === key && x.type === 'Boolean');
                        if (found >= 0) {
                            result[key] = parseBoolean(value);
                        }
                    }
                }
            });
            return result;
        }
        if (DataModel.prototype.cast !== cast) {
            Object.assign(DataModel.prototype, {
                cast
            });
        }

    }
}

export {
    EnableStrictBooleanConversion
}