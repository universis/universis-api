/**
 * @license
 * Universis Project Version 1.0
 * Copyright (c) 2018, Universis Project All rights reserved
 *
 * Use of this source code is governed by an LGPL 3.0 license that can be
 * found in the LICENSE file at https://gitlab.com/universis/universis-api/raw/master/LICENSE
 */
import Student from "../models/student-model";
import Instructor from "../models/instructor-model";
import {HttpNotFoundError, HttpForbiddenError} from "@themost/common";
/**
 * Gets or sets the interactive user
 * @name Request#student
 * @type Student
 * @memberOf Request
 */
/**
 * @returns {RequestHandler}
 */
export function interactiveStudent() {
    return (req, res, next) => {
        if (req.student) {
            return next();
        }
        Student.getMe(req.context).getTypedItem().then ( student => {
            // set student to current request params
            req.student = student;
            return next();
        }).catch(err => {
            return next(err);
        });
    };
}
/**
* Gets or sets the interactive user
* @name Request#student
* @type Student
* @memberOf Request
*/
/**
 * @returns {RequestHandler}
 */
export function tryStudent() {
    return (req, res, next) => {
        if (req.student) {
            return next();
        }
        if (/^me$/ig.test(req.params.id)) {
            return interactiveStudent(req, res, next);
        } else {
            req.context.model('Student').where('id').equal(req.params.id).getTypedItem().then(student => {
                // set student to current request params
                req.student = student;
                return next();
            }).catch(err => {
                return next(err);
            });
        }
    };
}

export function validateStudent() {
    return (req, res, next) => {
        if (req.student == null) {
            return next(new HttpForbiddenError('Student cannot be found or is inaccessible.'));
        }
        return next();
    };
}

/**
 * Gets or sets the interactive instructor
 * @name Request#instructor
 * @type Instructor
 * @memberOf Request
 */
/**
 * @returns {RequestHandler}
 */
export function interactiveInstructor() {
    return (req, res, next) => {
        if (req.instructor) {
            return next();
        }
        Instructor.getMe(req.context).then(getInstructor => {
            return getInstructor.getTypedItem().then(instructor => {
                // noinspection JSValidateTypes
                req.instructor = instructor;
                return next();
            })
        }).catch(err => {
            return next(err);
        });
    };
}
