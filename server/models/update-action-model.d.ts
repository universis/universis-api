import Action = require("./action-model");

/**
 * @class
 */
declare class UpdateAction extends Action {

     public initiator?: Action;

}

export = UpdateAction;
