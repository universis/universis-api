import app from '../server/app';
import {ExpressDataApplication, ExpressDataContext} from "@themost/express";
import {TestUtils} from '../server/utils';
import Rule from '../server/models/rule-model';
import _ from "lodash";
import {ValidationResult} from "../server/errors";

const executeInTransaction = TestUtils.executeInTransaction;

describe('YearMeanGradeRule', () => {
    /**
     * @type {ExpressDataContext}
     */
    let context;
    beforeAll(done => {
        /**
         * @type {ExpressDataApplication}
         */
        const app1 = app.get(ExpressDataApplication.name);
        context = app1.createContext();
        // disable sql logging
        process.env.NODE_ENV = 'test';
        return done();
    });
    afterAll(done => {
        if (context) {
            return context.finalize(() => done());
        }
        // re-enable development env

        return done();
    });

    const YEAR_MEAN_GRADE_RULE =
        {
            "id": 329650,
            "target": "3957",
            "targetType": "Scholarship",
            "refersTo": "YearMeanGrade",
            "additionalType": "ScholarshipRule",
            "targetTypeIdentifier": 109,
            "refersToIdentifier": 90,
            "additionalTypeIdentifier": 13,
            "checkValues": null,
            "ruleOperator": 5,
            "value1": "2015",
            "value2": "2019",
            "value3": ".651",
            "value4": "20",
            "value5": "1;4#2;5#-1;10",
            "value6": "-1",
            "value7": null,
            "value8": null,
            "value9": null,
            "value10": null,
            "value11": null,
            "value12": null,
            "value13": null,
            "ruleExpression": null,
            "programSpecialty": null,
            "dateModified": "2021-03-18T14:53:50.410Z"
        };

    const MEAN_GRADE_RULE =
        {
            "id": 150833,
            "target": "3689",
            "targetType": "Scholarship",
            "refersTo": "MeanGrade",
            "additionalType": "ScholarshipRule",
            "targetTypeIdentifier": 109,
            "refersToIdentifier": 80,
            "additionalTypeIdentifier": 13,
            "checkValues": "1,2,3,4",
            "ruleOperator": null,
            "value1": "2017",
            "value2": null,
            "value3": ".651",
            "value4": "1",
            "value5": "1;10#2;2",
            "value6": "-1",
            "value7": null,
            "value8": null,
            "value9": null,
            "value10": null,
            "value11": null,
            "value12": null,
            "value13": null,
            "ruleExpression": null,
            "programSpecialty": null,
            "dateModified": "2021-03-18T14:53:50.410Z"
        };

    it('should validate year mean grade rule', async () => {
        // await executeInTransaction(context, async () => {
        /**
         *
         * @type {YearMeanGradeRule}
         */


        return await new Promise((resolve, reject) => {

            let rule = context.model('YearMeanGradeRule').convert(YEAR_MEAN_GRADE_RULE);
            let data = {
                student: {id: 264415}
            };
            // rule.validate(data, function (err, result) {
            //     if (err) {
            //         return reject(err);
            //     }
            //     console.log(JSON.stringify(result, null, 4));
            //     return resolve();
            // });
            rule = context.model('MeanGradeRule').convert(MEAN_GRADE_RULE);
            data = {
                student: {id: 310775}
            };
            rule.validate(data, function (err, result) {
                if (err) {
                    return reject(err);
                }
                console.log(JSON.stringify(result, null, 4));
                return resolve();
            });
        });

        //});
    });
});
