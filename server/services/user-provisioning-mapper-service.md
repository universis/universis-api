
# UserProvisioningMapperService

`UserProvisioningMapperService` provides a different approach while getting active user based on the queried token info. 

`PersonAttributeMapperService` is a subclass of `UserProvisioningMapperService` will try to map active user based on an attribute defined in token info. This attribute is a part of a person record e.g. student institute identifier, student unique identifier, instructor unique identifier or any other attribute which may identify a user associated to a person entity.

## Configuration

Register `PersonAttributeMapperService` as strategy of `UserProvisioningMapperService` under `services` section of application configuration.

    {
      "serviceType": "./services/user-provisioning-mapper-service#UserProvisioningMapperService",
      "strategyType": "./services/user-provisioning-mapper-service#PersonAttributeMapperService"
    }

Define user provisioning service configuration under `settings/universis/userProvisioning` for getting an attribute from user token based on the assigned scope.

    "universis": {
      "userProvisioning": [
        {
          "scope": "students",
          "userAttribute": "studentInstituteIdentifier",
          "tokenAttribute": "studentInstituteIdentifier"
        },
        {
          "scope": "teachers",
          "userAttribute": "uniqueIdentifier",
          "tokenAttribute": "globalIdentifier",
          "required": true
        }
    ]

where the `userAttribute` is the attribute of a student that is going to be used for getting a record e.g. studentIdentifier, studentInstituteIdentifier, uniqueIdentifier etc

The `tokenAttribute` is the name of an attribute that exists in a user token e.g. studentInstituteIdentifier, globalIdentifier etc

Use `required` attribute to define if the presence of a user token attribute is required for retreiving an entity.