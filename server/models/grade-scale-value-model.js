import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';

/**
 * @class
 
 * @property {number} id
 * @property {string} name
 * @property {string} alternateName
 * @property {GradeScale|any} gradeScale
 * @property {number} valueFrom
 * @property {number} valueTo
 * @property {number} exactValue
 * @augments {DataObject}
 */
@EdmMapping.entityType('GradeScaleValue')
class GradeScaleValue extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = GradeScaleValue;