{
  "$schema": "https://themost-framework.github.io/themost/models/2018/2/schema.json",
  "@id": "https://universis.io/schemas/StudentThesis",
  "name": "StudentThesis",
  "title": "Εργασίες φοιτητών",
  "hidden": false,
  "sealed": false,
  "version": "0.2",
  "source": "StudentThesisBase",
  "view": "StudentThesisData",
  "fields": [
    {
      "name": "id",
      "title": "Κωδικός",
      "description": "Ο μοναδικός κωδικός του αντικειμένου",
      "nullable": false,
      "type": "Guid",
      "primary": true,
      "value": "javascript:return this.newGuid();"
    },
    {
      "name": "thesis",
      "title": "Εργασία",
      "description": "Η εργασία",
      "type": "Thesis",
      "nullable": false,
      "editable": false
    },
    {
      "name": "student",
      "title": "Φοιτητής",
      "description": "Ο φοιτητής",
      "type": "Student",
      "nullable": false,
      "editable": false
    },
    {
      "name": "dateCompleted",
      "title": "Ημερομηνία ολοκλήρωσης",
      "description": "Η ημερομηνία ολοκλήρωσης της εργασίας",
      "type": "DateTime",
      "nullable": true,
      "size": 4
    },
    {
      "name": "grade",
      "title": "Βαθμός",
      "description": "Βαθμός (0-1)",
      "type": "Number",
      "size": 4
    },
    {
      "name": "isPassed",
      "title": "Προβιβάσιμος",
      "description": "Ο βαθμός της εργασίας έχει περαστεί.",
      "type": "Number",
      "readonly": true,
      "insertable": false,
      "editable": false,
      "query": [
        {
          "$lookup": {
            "from": "Thesis",
            "localField": "thesis",
            "foreignField": "id",
            "as": "thesis"
          }
        },
        {
          "$lookup": {
            "from": "GradeScale",
            "localField": "$thesis.gradeScale",
            "foreignField": "id",
            "as": "gradeScale"
          }
        },
        {
          "$project": {
            "isPassed": {
              "$cond": [
                {
                    "$gte": [
                        {
                          "$ifNull": [
                            "$grade",
                            0
                          ]
                        },
                        {
                          "$ifNull": [
                            "$gradeScale.scaleBase",
                            0
                          ]
                        }
                    ]
                },
                1,
                0
              ]
            }
          }
        }
      ]
    },
    {
      "name": "formattedGrade",
      "title": "Βαθμός (στην κλίμακα)",
      "description": "Βαθμός (ανηγμένος στην κλίμακα βαθμολογίας της εργασίας).",
      "type": "Text",
      "readonly": true,
      "insertable": false,
      "editable": false,
      "query": [
        {
          "$lookup": {
            "from": "Thesis",
            "localField": "thesis",
            "foreignField": "id",
            "as": "thesis"
          }
        },
        {
          "$lookup": {
            "from": "GradeScale",
            "localField": "$thesis.gradeScale",
            "foreignField": "id",
            "as": "gradeScale"
          }
        },
        {
          "$lookup": {
              "@help": "pseudo-SQL: LEFT JOIN GradeScaleValue AS gradeScaleValue ON gradeScale.id = gradeScaleValue.gradeScale AND (gradeScale.scaleType = 1 OR gradeScale.scaleType = 3) AND (gradeScaleValue.valueTo >= grade AND gradeScaleValue.valueFrom <= grade)",
              "from": "GradeScaleValue",
              "pipeline": [
                  {
                      "$match": {
                          "$expr": {
                              "$and": [
                                { "$eq": [ "$gradeScale.id", "$gradeScaleValue.gradeScale" ] },
                                { 
                                  "$or": [
                                    { "$eq": [ "$gradeScale.scaleType", 1 ] },
                                    { "$eq": [ "$gradeScale.scaleType", 3 ] }
                                  ]
                                },
                                { "$ne": [ "$grade", null ] },
                                {
                                  "$and": [
                                    { "$gte": [
                                        "$gradeScaleValue.valueTo",
                                        { "$ifNull": [  "$grade", 0 ] }
                                      ]
                                    },
                                    { "$lte": [
                                        "$gradeScaleValue.valueFrom",
                                        { "$ifNull": [  "$grade", 0 ] }
                                      ]
                                    }
                                  ]
                                }
                              ]
                          }
                      }
                  }
              ],
              "as": "gradeScaleValue"
          }
        },
        {
          "@help": "SELECT CASE WHEN gradeScale.scaleType = 3 OR gradeScale.scaleType = 1 THEN gradeScaleValue.name ELSE grade / ISNULL(gradeScale.scaleFactor, 1) END",
          "$project": {
            "formattedGrade": {
              "$cond": [
                {
                  "$or": [
                    { "$eq": [ "$gradeScale.scaleType", 3 ] },
                    { "$eq": [ "$gradeScale.scaleType", 1 ] }
                  ]
                },
                "$gradeScaleValue.alternateName",
                {
                  "$toString": [
                    {
                      "$div": [
                        "$grade",
                        { "$ifNull": [  "$gradeScale.scaleFactor", 1 ] }
                      ]
                    }
                  ]
                }
              ]
            }
          }
        }
      ]
    },
    {
      "name": "dateModified",
      "title": "Ημερομηνία τροποποίησης",
      "description": "Ημερομηνία τροποποίησης",
      "type": "DateTime",
      "nullable": false,
      "value": "javascript:return this.now();",
      "calculation": "javascript:return this.now();"
    },
    {
      "name": "results",
      "description": "A collection of results associated with this thesis.",
      "type": "StudentThesisResult",
      "many": true,
      "mapping": {
        "parentModel": "StudentThesis",
        "childModel": "StudentThesisResult",
        "parentField": "id",
        "childField": "studentThesis",
        "cascade": "none",
        "associationType": "association"
      }
    }
  ],
  "constraints": [
      {
          "type": "unique",
          "fields": [
              "thesis",
              "student"
          ]
      }
  ],
  "eventListeners": [
    {
      "type": "@themost/data/previous-state-listener"
    },
    {
      "type": "./listeners/student-thesis-listener"
    }
  ],
  "privileges": [
    {
      "mask": 15,
      "type": "global",
      "account": "Administrators"
    },
    {
      "mask": 15,
      "type": "global"
    },
    {
      "mask": 1,
      "type": "self",
      "filter": "student eq student()",
      "account": "Students"
    },
    {
      "mask": 1,
      "type": "self",
      "filter": "thesis/instructor eq instructor()",
      "account": "Instructors"
    },
    {
      "mask": 15,
      "type": "self",
      "account": "Registrar",
      "filter": "student/department eq departments()"
    }
  ],
  "views": [
    {
      "name": "InstructorThesisStudents",
      "description": "This view contains the fields that is going to used for viewing thesis students.",
      "fields": [
        {
          "name": "thesis"
        },
        {
          "name": "student",
          "mapping": {
            "options": {
              "$select": "StudentSummary"
            }
          }
        },
        {
          "name": "dateCompleted"
        },
        {
          "name": "grade"
        },
        {
          "name": "isPassed"
        },
        {
          "name": "formattedGrade"
        },
        {
          "name": "dateModified"
        }
      ],
      "privileges": [
        {
          "mask": 1,
          "type": "self",
          "filter": "thesis/instructor eq instructor()",
          "account": "Instructors"
        }
      ]
    }
  ]
}
