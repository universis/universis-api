import {EdmMapping} from '@themost/data/odata';
import {DataError} from '@themost/common';
let UpdateAction = require('./update-action-model');
/**
 * @class
 
 * @property {StudyProgramSpecialty|any} currentSpecialty
 * @property {StudyProgramSpecialty|any} specialty
 * @property {AcademicYear|any} academicYear
 * @property {AcademicPeriod|any} academicPeriod
 * @property {number} id
 */
@EdmMapping.entityType('UpdateSpecialtyAction')
class UpdateSpecialtyAction extends UpdateAction {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    async complete() {
        // validate action status
        const actionStatus = this.property('actionStatus').getItem();
        if (actionStatus.alternate !== 'ActiveActionStatus') {
            throw new DataError('ESTATUS', 'Action cannot be completed due to its state.', null, 'UpdateSpecialtyAction');
        }
    }

}
module.exports = UpdateSpecialtyAction;
