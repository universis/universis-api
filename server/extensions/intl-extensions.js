// Compatibility implementation of the ECMAScript Internationalization API (ECMA-402) for JavaScript
// https://github.com/andyearnshaw/Intl.js#readme
import IntlPolyfill from 'intl';
Intl.NumberFormat   = IntlPolyfill.NumberFormat;
Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;
// extend Number.prototype for Node.js
Object.assign(Number.prototype, {
    /**
     * @this Number
     * @param {string|string[]=} locales
     * @param {Intl.NumberFormatOptions} options
     * @returns {string}
     */
   toLocaleString: function(locales, options) {
       return new Intl.NumberFormat(locales, options).format(this);
   }
});