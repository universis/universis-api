import {ApplicationService} from "@themost/common";
import {ResponseFormatter} from "@themost/express";
import {XlsxContentType} from "../middlewares/xls";
import {XlsxFormatter} from "../formatters/xlsx.formatter";

export class XlsxFormatterService extends ApplicationService {
    /**
     *
     * @param {ExpressDataApplication} app
     */
    constructor(app) {
        super(app);
        if(!app.hasService(ResponseFormatter)){
            app.useService(ResponseFormatter)
        }
        app.getService(ResponseFormatter).formatters.set(XlsxContentType, XlsxFormatter)
    }
}

