import {EdmMapping, EdmType} from "@themost/data";
import {Event} from "@universis/events";
import {DataConflictError, ValidationResult} from "../errors";
import {PercentileRanking} from "@universis/percentile-ranking";
import {DataError, DataNotFoundError, HttpServerError, TraceUtils} from "@themost/common";
import _ from 'lodash';
import * as studentCourseListener from '../listeners/student-course-listener';

@EdmMapping.entityType('GraduationEvent')
class GraduationEventModel extends Event {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.param('students', EdmType.CollectionOf('GraduationRequestAction'), false, true)
    @EdmMapping.action('calculatePercentileRanksByGradYear', 'Object')
    async calculatePercentileRanksByGradYear(students) {
        let graduationEvent = await this.context.model('GraduationEvent').where('id').equal(this.getId()).getTypedItem();

        // cannot calculate percentile rank for non-completed events
        if (graduationEvent?.eventStatus === 'EventCompleted') {
            throw new DataConflictError('Cannot calculate percentile ranks for non-completed graduation events', null, 'GraduationEvent');
        }

        // get requests made for this graduation event by the students passed as a param
        let requests = await this.context.model('GraduationRequestAction')
            .where('id').in(students)
            .expand('student')
            .getTypedItems();

        // get all student requests made for the graduation event
        let studentRequests = await this.context.model('GraduationRequestActions')
            .where('graduationEvent').equal(graduationEvent?.id)
            .and('actionStatus/alternateName').equal('CompletedActionStatus')
            .and('student/graduationGrade').notEqual(null)
            .and('student/studentStatus/alternateName').equal('graduated')
            .expand({
                'name': 'student',
                'options': {
                    '$expand': 'graduationYear'
                }
            })
            .getTypedItems();


        let distinctGraduationYears = [...new Set(studentRequests.map(x => x?.student?.graduationYear?.id ?? x?.student?.graduationYear))];

        let requestIds = studentRequests.map(x => x.id);
        // check if all students sent as param actually
        // have a completed request for this graduation event
        if (!requests.every(x => requestIds.includes(x.id))) {
            throw new DataConflictError("Not all student requests belong to this graduation event", null, 'GraduationEvent');
        }
        let department = await this.context.model('Department').where('id').equal(graduationEvent?.organizer).expand({
            'name': 'organization',
            'options': {
                '$expand': 'instituteConfiguration'
            }
        }).getTypedItem();
        let instituteConfiguration = department?.organization?.instituteConfiguration;
        studentRequests = studentRequests.map(x => x.student);

        studentRequests = [...studentRequests, ...(await this.context.model('Students')
                .where('department').equal(graduationEvent?.organizer?.id ?? graduationEvent?.organizer)
                .and('studentStatus/alternateName').equal('graduated')
                .and('graduationYear').in(distinctGraduationYears)
                .and('id').notIn(studentRequests.map(x => x.id))
                .getItems()
        )];

        let last_year = graduationEvent?.graduationYear?.id ?? graduationEvent?.graduationYear;
        let last_period = graduationEvent?.graduationPeriod?.id ?? graduationEvent?.graduationPeriod;
        // check if there is a minimum threshold for percentile ranks calculation
        // and if the current data satisfies that threshold
        let res = [];
        for (let i =0; i < distinctGraduationYears.length; ++i) {
            let studentsInYear = studentRequests.filter(x=> x?.graduationYear?.id === distinctGraduationYears[i] ?? x?.graduationYear === distinctGraduationYears[i])
            if (Array.isArray(studentsInYear) && studentsInYear.length < instituteConfiguration?.percentileRankItemThreshold) {
                let graduationEvents = await this._getGraduationEvents(graduationEvent);
                for (const [eventIndex, event] of graduationEvents.entries()) {
                    //const requests = await this._getEventRequests(event);
                    //let studentsWithRequest = requests.map(x => x?.student?.id ?? x?.student)
                    studentsInYear = [...studentsInYear, ...(await this.context.model('Students')
                        .where('studentStatus/alternateName').equal('graduated')
                        .and('graduationYear').equal(event?.graduationYear?.id ?? event?.graduationYear)
                        .and('graduationPeriod').equal(event?.graduationPeriod?.id ?? event?.graduationPeriod)
                        .and('department').equal(graduationEvent?.organizer?.id ?? graduationEvent?.organizer)
                        .and('id').notIn(studentsInYear.map(x => x.id))
                        .getItems())];
                    let otherGraduationEventsInThatYear = graduationEvents.filter((item, index) => item.graduationYear.id === event.graduationYear.id && item.id !== event.id && index > eventIndex).length
                    last_year = event?.graduationYear?.id ?? event?.graduationYear;
                    last_period = event?.graduationPeriod?.id ?? event?.graduationPeriod;
                    studentsInYear = studentsInYear.filter((value, index, self) => self.findIndex((m) => m?.id === value?.id) === index);
                    if (studentsInYear.length >= instituteConfiguration?.percentileRankItemThreshold && !otherGraduationEventsInThatYear) {
                        break;
                    }
                }
            }
            let s = studentsInYear.filter((value, index, self) => self.findIndex((m) => m?.id === value?.id) === index)
            if (s.length < instituteConfiguration?.percentileRankItemThreshold) {
                s = await this._getGraduatesFromPreviousYears(s, (graduationEvent?.organizer?.id ?? graduationEvent?.organizer), instituteConfiguration, last_year, last_period);
            }
            let grades = s.map(x => {
                return {
                    grade: Math.round(x.graduationGrade * 10),
                    student: x.id,
                    percentileRank: x.graduationRankByGraduationYear
                }
            });
            switch (instituteConfiguration?.percentileRankMethod) {
                case 1:
                    PercentileRanking.simplePercentileCalculation(grades);
                    break;
                case 2:
                    PercentileRanking.complexPercentileCalculation(grades);
                    break;
                default:
                    throw new HttpServerError("Percentile rank method not implemented yet.", "The percentile rank your institute has selected is not yet implemented.");
            }
            const filteredStudents = grades.filter(x => requests.map(r => r.student.id).includes(x.student));
            for (const item of filteredStudents) {
                let {id, percentileRank} = item;
                let {student} = requests.find(x => x?.student?.id === item.student ?? x?.student === item.student)
                res.push({...student, ...id, graduationRankByGraduationYear: parseFloat(percentileRank.toFixed(4))});
            }
        }
        try {
            return this.context.model('Students').silent().save(res);
        } catch (e) {
            TraceUtils.error(e);
        }
    }

    /**
     *
     * @param {Array<any>} s
     * @param {number} organizer
     * @param {Object} instituteConfiguration
     * @param {number} last_year
     * @param {number} last_period
     * @returns {Promise<Array<any>>}
     * @private
     */
    async _getGraduatesFromPreviousYears(s, organizer, instituteConfiguration, last_year, last_period){
        while (s.length < instituteConfiguration?.percentileRankItemThreshold) {
            let ids = s.map(x => x.id)
            last_year = last_period === 1 ? --last_year : last_year
            last_period = last_period === 1 ? 2 : --last_period
            s = [...s, ...(await this.context.model('Students')
                    .where('studentStatus/alternateName').equal('graduated')
                    .and('graduationYear').equal(last_year)
                    .and('graduationPeriod').equal(last_period)
                    .and('department').equal(organizer)
                    .and('id').notIn(ids)
                    .take(instituteConfiguration?.percentileRankItemThreshold - s.length)
                    .getItems()
            )];
        }
        return s;
    }

    async _getGraduatesEnrolledInPreviousYears(s, organizer, instituteConfiguration, last_year) {
        while (s.length < instituteConfiguration?.percentileRankItemThreshold) {
            let ids = s.map(x => x.id)
            --last_year;
            s = [...s, ...(await this.context.model('Students')
                    .where('studentStatus/alternateName').equal('graduated')
                    .and('inscriptionYear').equal(last_year)
                    .and('department').equal(organizer)
                    .and('id').notIn(ids)
                    .getItems()
            )];
        }
        return s;
    }

    async _getGraduationEvents(graduationEvent){
        return await this.context.model('GraduationEvents')
            .where('organizer').equal(graduationEvent.organizer)
            .and('id').notEqual(graduationEvent?.id)
            .and('graduationYear').lowerThan(graduationEvent?.graduationYear?.id ?? graduationEvent?.graduationYear)
            .orderByDescending('graduationYear').thenByDescending('graduationPeriod')
            .getTypedItems();
    }

    @EdmMapping.param('students', EdmType.CollectionOf('GraduationRequestAction'), false, true)
    @EdmMapping.action('calculatePercentileRanksByInscrYear', 'Object')
    async calculatePercentileRanksByInscrYear(students) {
        let graduationEvent = await this.context.model('GraduationEvent').where('id').equal(this.getId()).getTypedItem();

        // cannot calculate percentile rank for non-completed events
        if (graduationEvent?.eventStatus === 'EventCompleted') {
            throw new DataConflictError('Cannot calculate percentile ranks for non-completed graduation events', null, 'GraduationEvent');
        }

        // get requests made for this graduation event by the students passed as a param
        let requests = await this.context.model('GraduationRequestAction')
            .where('id').in(students)
            .expand('student')
            .getTypedItems();

        // get all student requests made for the graduation event
        let studentRequests = await this.context.model('GraduationRequestActions')
            .where('graduationEvent').equal(graduationEvent?.id)
            .and('actionStatus/alternateName').equal('CompletedActionStatus')
            .and('student/graduationGrade').notEqual(null)
            .and('student/studentStatus/alternateName').equal('graduated')
            .expand({
                'name': 'student',
                'options': {
                    '$expand': 'inscriptionYear'
                }
            })
            .getTypedItems();


        let distinctInscriptionYears = [...new Set(studentRequests.map(x => x?.student?.inscriptionYear?.id ?? x?.student?.inscriptionYear))];

        let requestIds = studentRequests.map(x => x.id);
        // check if all students sent as param actually
        // have a completed request for this graduation event
        if (!requests.every(x => requestIds.includes(x.id))) {
            throw new DataConflictError("Not all student requests belong to this graduation event", null, 'GraduationEvent');
        }
        let department = await this.context.model('Department').where('id').equal(graduationEvent?.organizer).expand({
            'name': 'organization',
            'options': {
                '$expand': 'instituteConfiguration'
            }
        }).getTypedItem();
        let instituteConfiguration = department?.organization?.instituteConfiguration;
        studentRequests = studentRequests.map(x => x.student);

        studentRequests = [...studentRequests, ...(await this.context.model('Students')
                .where('department').equal(graduationEvent?.organizer?.id ?? graduationEvent?.organizer)
                .and('studentStatus/alternateName').equal('graduated')
                .and('inscriptionYear').in(distinctInscriptionYears)
                .and('id').notIn(studentRequests.map(x => x.id))
                .getItems()
        )];
        // check if there is a minimum threshold for percentile ranks calculation
        // and if the current data satisfies that threshold
        let res = [];
        for (let i =0; i < distinctInscriptionYears.length; ++i) {
            let studentsInYear = studentRequests.filter(x=> x?.inscriptionYear?.id === distinctInscriptionYears[i] ?? x?.inscriptionYear === distinctInscriptionYears[i])
            if (Array.isArray(studentsInYear) && studentsInYear.length < instituteConfiguration?.percentileRankItemThreshold) {
                studentsInYear = await this._getGraduatesEnrolledInPreviousYears(studentsInYear, (graduationEvent?.organizer?.id ?? graduationEvent?.organizer), instituteConfiguration, distinctInscriptionYears[i]);
                studentsInYear = studentsInYear.filter((value, index, self) => self.findIndex((m) => m?.id === value?.id) === index);
            }
            let s = studentsInYear.filter((value, index, self) => self.findIndex((m) => m?.id === value?.id) === index)
            let grades = s.map(x => {
                return {
                    grade: Math.round(x.graduationGrade * 10),
                    student: x.id,
                    percentileRank: x.graduationRankByInscriptionYear
                }
            });
            switch (instituteConfiguration?.percentileRankMethod) {
                case 1:
                    PercentileRanking.simplePercentileCalculation(grades);
                    break;
                case 2:
                    PercentileRanking.complexPercentileCalculation(grades);
                    break;
                default:
                    throw new HttpServerError("Percentile rank method not implemented yet.", "The percentile rank your institute has selected is not yet implemented.");
            }
            const filteredStudents = grades.filter(x => requests.map(r => r.student.id).includes(x.student));
            for (const item of filteredStudents) {
                let {id, percentileRank} = item;
                let {student} = requests.find(x => x?.student?.id === item.student ?? x?.student === item.student)
                res.push({...student, ...id, graduationRankByInscriptionYear: parseFloat(percentileRank.toFixed(4))});
            }
        }
        try {
            return this.context.model('Students').silent().save(res);
        } catch (e) {
            TraceUtils.error(e);
        }
    }


    @EdmMapping.param('requests', EdmType.CollectionOf('GraduationRequestAction'), true, true)
    @EdmMapping.action('calculatePercentileRanks', 'PercentileRankAction')
    async calculateClassPercentileRanks(requests) {
        // get and validate graduationEvent
        let graduationEvent = await this.context.model('GraduationEvent')
            .where('id').equal(this.getId())
            .select('id', 'eventStatus/alternateName as eventStatus', 'organizer/organization as institute')
            .getItem();
        if (graduationEvent == null) {
            throw new DataNotFoundError('The specified graduation event cannot be found or is inaccessible.');
        }
        // cannot calculate percentile rank for non-completed events
        if (graduationEvent.eventStatus !== 'EventCompleted') {
            throw new DataConflictError('Cannot calculate percentile ranks for non-completed graduation events', null, 'GraduationEvent');
        }
        // validate that there is no active calculation action for this event
        // at this current moment
        // TODO: Handle extreme cases like api-restarts (hung ActiveActionStatus)
        const inProgressAction = await this.context.model('PercentileRankAction')
            .where('targetType').equal('GraduationEvent')
            .and('targetIdentifier').equal(graduationEvent.id.toString())
            .and('actionStatus/alternateName').equal('ActiveActionStatus')
            .and('rankType/alternateName').equal('coursePercentileRank')
            .select('id')
            .silent()
            .count();
        if (inProgressAction) {
            throw new DataConflictError('Only one student course percentile rank calculation action can be active per graduation event.');
        }
        // get institute configuration
        const instituteConfiguration = await this.context.model('InstituteConfiguration')
            .where('institute').equal(graduationEvent.institute)
            .select('percentileRankMethod', 'percentileRankItemThreshold')
            .expand('percentileRankMethod')
            .silent()
            .getItem();
        if (instituteConfiguration == null) {
            throw new DataNotFoundError('The institute configuration cannot be found.');
        }
        // and validate calculation method before starting the async process
        const availableCalculationMethods = ['simple', 'complex'];
        if (!availableCalculationMethods.includes(instituteConfiguration.percentileRankMethod
            && instituteConfiguration.percentileRankMethod.alternateName)) {
            throw new DataConflictError('The specified percentile rank method is not yet implemented.');
        }

        // get all student requests made for the graduation event
        const studentRequests = await this.context.model('GraduationRequestActions')
            .where('graduationEvent').equal(graduationEvent.id)
            .and('actionStatus/alternateName').equal('CompletedActionStatus')
            .and('student/graduationGrade').notEqual(null)
            .and('student/studentStatus/alternateName').equal('graduated')
            .and('id').in(requests)
            .select('id', 'student')
            .silent()
            .getAllItems();
        if (studentRequests == null || studentRequests.length === 0) {
            throw new DataConflictError('There are no available student requests for the specified graduation event. The calculation cannot happen.');
        }

        // get all passed student courses (exclude exemption)
        // that do not have percentileRanks already calculated
        const studentCourses = await this.context.model('StudentCourse')
            .where('student').in(studentRequests.map(x => x?.student?.id ?? x?.student))
            .and('isPassed').equal(true)
            .and('percentileRank').equal(null)
            .and('registrationType').notEqual(1)
            .select('id', 'student', 'course', 'gradeYear',
                'gradePeriod', 'calculated', 'calculateGrade', 'calculateUnits')
            .flatten()
            .silent()
            .getAllItems();
        
        if (studentCourses == null || studentCourses.length === 0) {
            return new ValidationResult(true, 'ALREADY_CALCULATED', 
                'All student course percentile ranks of the specified graduation event have already been calculated');
        }
        // prepare the action
        const action = {
            totalStudents: studentRequests.length,
            targetType: 'GraduationEvent',
            targetIdentifier: this.getId().toString(),
            rankType: {
                alternateName: 'coursePercentileRank'
            }
        };
        // save it
        const actionResult = await this.context.model('PercentileRankAction').silent().save(action);
        // begin async calculations
        this.calculatePercentileRanksForStudentCourse(this.context, action, studentCourses, studentRequests, instituteConfiguration, this.getId());
        // and return the (active) action
        return actionResult;
    }

    calculatePercentileRanksForStudentCourse(appContext, action, studentCourses, studentRequests, instituteConfiguration, graduationEvent) {
        // create a new context
        const app = appContext.getApplication();
        const context = app.createContext();
        context.user = appContext.user;
        const self = this;
        (async function () {
            // get distinct course classes
            let distinctCourseClasses = _.groupBy(studentCourses, (item) => {
                return [item.course, item.gradeYear, item.gradePeriod];
            });
            let res = [];
            const firstTake = 10;
            const maxTake = 100;
            const sampleThreshold = instituteConfiguration?.percentileRankItemThreshold || 0;
            for (const [classInformation, courseClasses] of Object.entries(distinctCourseClasses)) {
                const studentClassesSampleIds = [];
                let currentSampleSize = 0;
                // fetch course, gradeYear, gradePeriod
                const information = classInformation.split(',');
                const classData = {
                    course: information[0],
                    gradeYear: information[1],
                    gradePeriod: information[2]
                }
                // fetch a starting statistical sample
                const statisticalSample = await self.getStatisticalSample(/*skip=*/0, /*take=*/firstTake, context, classData);
                
                for (const sample of statisticalSample) {
                    // push sample
                    studentClassesSampleIds.push(sample.courseClass)
                    // increment current size
                    currentSampleSize += sample.total;
                    // until currentSampleSize satisfies the institute threshold
                    if (currentSampleSize >= sampleThreshold) {
                        break;
                    }
                }
                // if the sampleThreshold has not been satisfied by the first batch
                if (currentSampleSize < sampleThreshold) {
                    // get all remaining records
                    const remainingStatisticalSample = await self.getStatisticalSample(/*skip=*/firstTake, /*take=*/maxTake, context, classData);
                    for (const sample of remainingStatisticalSample) {
                        // push sample
                        studentClassesSampleIds.push(sample.courseClass)
                        // increment current size
                        currentSampleSize += sample.total;
                        // until currentSampleSize satisfies the institute threshold
                        if (currentSampleSize >= sampleThreshold) {
                            break;
                        }
                    }
                }
                // get classes
                let studentClasses = await context.model('StudentCourseClasses')
                    .where('courseClass').in(studentClassesSampleIds)
                    .and('finalGrade').notEqual(null)
                    .and('isPassed').equal(true)
                    .select('course', 'finalGrade', 'student', 'percentileRank')
                    .silent()
                    .getAllItems();

                // get unique student course class objects
                let grades = studentClasses.map(x => {return {
                    grade: x.finalGrade,
                    student: x.student,
                    percentileRank: x.percentileRank,
                    course: x.course
                }});
                // calculate percentile ranks based on percentileRankMethod
                switch (instituteConfiguration?.percentileRankMethod?.alternateName) {
                    case 'simple':
                        PercentileRanking.simplePercentileCalculation(grades);
                        break;
                    case 'complex':
                        PercentileRanking.complexPercentileCalculation(grades);
                        break;
                    default:
                        throw new HttpServerError("Percentile rank method not implemented yet.", "The percentile rank your institute has selected is not yet implemented.");
                }
                // get percentile ranks of provided students to the action
                const filteredStudents = grades.filter(x => studentRequests.map(r=> r?.student?.id ?? r?.student).includes(x.student));
                for (const item of filteredStudents) {
                    let {grade , percentileRank} = item;
                    // find student course
                    let sc = studentCourses.find(x => x.student === item.student && x.course === item.course);
                    if (sc) {
                        // and push to results
                        res.push({...sc, percentileRank: parseFloat(percentileRank.toFixed(4))});
                    }
                }
            }
            try {
                const StudentCourse = context.model('StudentCourse');
                // temporarily remove listeners of StudentCourse
                StudentCourse.removeListener('before.save', studentCourseListener.beforeSave);
                StudentCourse.removeListener('after.save', studentCourseListener.afterSave);
                // prepare results
                const results = res.map(item => {
                    return {
                        id: item.id,
                        percentileRank: item.percentileRank,
                        // note: provide calculated attributes to prevent extra calls
                        // while saving the object
                        calculated: item.calculated,
                        calculateGrade: item.calculateGrade,
                        calculateUnits: item.calculateUnits
                    }
                });
                for (const result of results) {
                    await new Promise((resolve) => {
                        // update student course percentile rank
                        return StudentCourse.silent().save(result).then(() => {
                            const studentCourse = studentCourses.find(course => course.id === result.id);
                            // and create a CalculatePercentileRanksActionResult
                            const calculationResult = {
                                action: action.id,
                                student: studentCourse.student,
                                course: studentCourse.course,
                                updated: true,
                                result: result.percentileRank
                            }
                            return context.model('PercentileRankActionResult').silent().save(calculationResult).then(() => {
                                return resolve();
                            }).catch(err => {
                                TraceUtils.error('An error occured while saving percentile ranking calculation result.')
                                TraceUtils.error(err);
                                return resolve();
                            })
                        }).catch(err => {
                            TraceUtils.error(`An error occured while trying to update percentile rank for studentCourse ${result.id}.`);
                            TraceUtils.error(err)
                            const studentCourse = studentCourses.find(course => course.id === result.id);
                            // and create a CalculatePercentileRanksActionResult
                            const calculationResult = {
                                action: action.id,
                                student: studentCourse.student,
                                course: studentCourse.course,
                                updated: false,
                                result: null,
                                description: JSON.stringify(err)
                            };
                            return context.model('PercentileRankActionResult').silent().save(calculationResult).then(() => {
                                return resolve();
                            }).catch(err => {
                                TraceUtils.error('An error occured while saving percentile ranking calculation result.')
                                TraceUtils.error(err);
                                return resolve();
                            });
                        });
                    });
                }
            } catch (e) {
                TraceUtils.error(e);
                throw e;
            }
        })().then(() => {
            context.finalize(() => {
                // mark action as completed
                action.actionStatus = {alternateName: 'CompletedActionStatus'};
                action.endTime = new Date();
                return context.model('PercentileRankAction').silent().save(action).then(() => {
                    }).catch(err => {
                        TraceUtils.error(`An error occured while trying to complete percentile ranks calculation action ${action.id}.`);
                        TraceUtils.error(err);
                    });
            });
        }).catch(err => {
            context.finalize(() => {
                // mark action as failed
                action.actionStatus = {alternateName: 'FailedActionStatus'};
                action.endTime = new Date();
                action.description = err.message;
                return context.model('PercentileRankAction').silent().save(action).then(() => {
                    }).catch(err => {
                        TraceUtils.error(`An error occured while trying to mark as failed percentile ranks calculation action ${action.id}.`);
                        TraceUtils.error(err);
                    });
            });
        });
    }

    getStatisticalSample(skip, take, context, data) {
        if (!(data.course && data.gradeYear && data.gradePeriod)) {
            return Promise.resolve([]);
        }
        skip = (typeof skip === 'number' && skip >= 0) ? skip : 0;
        take = typeof take === 'number' ? take : -1;
        return context.model('StudentCourseClass')
            .where('courseClass/year').equal(data.gradeYear)
            .and('courseClass/period').lowerOrEqual(data.gradePeriod)
            .or('courseClass/year').lowerThan(data.gradeYear)
            .prepare()
            .and('isPassed').equal(true)
            .and('courseClass/course').equal(data.course)
            .select('courseClass', 'courseClass/year as courseClassYear',
                'courseClass/period as courseClassPeriod', 'count(id) as total')
            .groupBy('courseClass', 'courseClass/year', 'courseClass/period')
            .orderByDescending('courseClass/year')
            .thenByDescending('courseClass/period')
            .take(take)
            .skip(skip)
            .silent()
            .getItems();
    }
}

module.exports = GraduationEventModel;
