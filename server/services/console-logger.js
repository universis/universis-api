export class ConsoleLogger {
    constructor() {
        // set default level
        this._level = ConsoleLogger.Levels.info;
        // init console with process stdout and stderr
        this._console = new console.Console(process.stdout, process.stderr);
    }

    /**
     * An enumeration of valid log levels
     */
    static get Levels() {
        return {
            error: 0,
            warn: 1,
            info: 2,
            verbose: 3,
            debug: 4
        }
    } 

    level(level) {
        // set level if the given level is valid
        if (Object.prototype.hasOwnProperty.call(ConsoleLogger.Levels, level)) {
            this._level = ConsoleLogger.Levels[level];
        }
        // otherwise throw error
        else {
            throw new Error('Invalid log level. Expected one of error, warn, info, verbose, debug.')
        }
    }
    
    log() {
        // use console.log
        this._console.log.apply(this._console, arguments);
    }

    error() {
        // use console.log
        this._console.error.apply(this._console, arguments);
    }

    info() {
        // exit if current log level is lower than info
        if (this._level < ConsoleLogger.Levels.info) {
            return;
        }
        this._console.log.apply(this._console, arguments);
    }

    warn() {
        // exit if current log level is lower than warn
        if (this._level < ConsoleLogger.Levels.warn) {
            return;
        }
        this._console.error.apply(this._console, arguments);
    }

    verbose() {
        // exit if current log level is lower than verbose
        if (this._level < this._levels.verbose) {
            return;
        }
        this._console.log.apply(this._console, arguments);
    }

    debug() {
        // exit if current log level is lower than debug
        if (this._level < ConsoleLogger.Levels.debug) {
            return;
        }
        this._console.log.apply(this._console, arguments);
    }
}
