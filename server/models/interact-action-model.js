import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
let Action = require('./action-model');
/**
 * @class
 
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('InteractAction')
class InteractAction extends Action {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = InteractAction;