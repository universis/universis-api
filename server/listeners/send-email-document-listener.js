import {ApplicationService, DataNotFoundError, TraceUtils} from "@themost/common";
import * as _ from 'lodash';
import {DataConfigurationStrategy} from "@themost/data";
import path from "path";
import mailer from "@themost/mailer";
import moment from 'moment';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {

    if (event.state!==1)
    {
        return callback();
    }
    // validate data
    if (event.target == null) {
        return callback();
    }
    const context = event.model.context;
    (async function () {

        // load RequestDocumentAction
        const request = await context.model('RequestDocumentAction').where('id').equal(event.target.id)
            .expand({
                    'name': 'student',
                    'options': {
                        '$expand': 'department,person'
                    }
                },
                {'name': 'object'},
                {'name': 'actionStatus'})
            .levels(5).getItem();

        if (request == null) {
            // request not found
            TraceUtils.warn(`Request with id ${event.target.id} cannot be found.`);
            throw new DataNotFoundError("Request not found")
        }
        // send email only if actionStatus is active
        if (request.actionStatus.alternateName !== 'ActiveActionStatus') {
            return;
        }

        // get request documentConfiguration
        if (request.object && request.object.alternateName) {
            // check if mailConfiguration exists for this documentConfiguration - dasherized e.g. draft-transcript
            const mailTemplate = await context.model('MailConfiguration').where('template').equal(_dasherize(request.object.alternateName))
                .silent().getItem();

            if (mailTemplate == null) {
                // mailTemplate does not exist
                return;
            }
            // if (request.student.person.email == null || (request.student.person.email && typeof request.student.person.email !== 'string')) {
            //     TraceUtils.warn(`Cannot send email for ${request.object.alternateName} . Student [${request.student.id}] email does not exist.`);
            //     throw new DataNotFoundError("Student email is missing");
            // }
            //build data required for mail template
            // get student courses
            const courses = await context.model('StudentCourse').where('student').equal(request.student.id).expand('course', 'semester', 'courseType', 'gradeExam').silent().getItems();
            // get student theses
            const theses = await context.model('StudentThesis').where('student').equal(request.student.id)
                .expand({
                        'name': 'results',
                        'options': {
                            '$expand': 'instructor',
                            '$orderby': 'index'
                        }
                    },
                    {
                        'name': 'thesis',
                        'options': {
                            '$expand': 'instructor',
                        }
                    },)
                .levels(5).getItems();

            const data = {
                'student': request.student,
                'courses': courses,
                'theses': theses
            };
            // send mail
            await new Promise((resolve, reject) => {
                return mailer.getMailer(context)
                    .test(true)
                    .template(mailTemplate.template).send(Object.assign(data, {
                        html: {
                            moment: moment
                        }
                    }), (err, data) => {
                        if (err) {
                            TraceUtils.error('An error occurred while sending student request document email:' + event.target.id);
                            return reject(err);
                        }
                        // set completed action status
                        Object.assign(event.target, {
                            actionStatus: {
                                alternateName: "CompletedActionStatus"
                            }
                        });
                        return context.model('RequestDocumentAction').silent().save(event.target, function (err) {
                            if (err) {
                                return reject(err);
                            }
                            return context.model('StudentMessage').silent().save({
                                student: event.target.student,
                                action: event.target.id,
                                subject: context.__('CompletedStudentRequestSubject'),
                                body: context.__('AttachmentFileCreated')
                            }).then(message => {
                                // create pdf and add attachment
                                //generate pdf
                                message = context.model('StudentMessage').convert(message);
                                 return  message.addHtmlAsAttachment(data.html).then(() => {
                                     return resolve();
                                 });
                            }).catch(err => {
                                return reject(err);
                            });
                        });
                    });
            });
        } else {
            return;
        }
    })().then(() => {
       return callback();
    }).catch(err => {
        TraceUtils.error(err);
        return callback(err);
    });
}

function _dasherize(s) {
    if (_.isString(s))
        return _.trim(s).replace(/[_\s]+/g, '-').replace(/([A-Z])/g, '-$1').replace(/-+/g, '-').replace(/^-/,'').toLowerCase();
    return s;
}

export class EmailDocumentAction extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // call super constructor
        super(app);
        // install service
        this.install();
    }

    install() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getApplication().getConfiguration().getStrategy(DataConfigurationStrategy);
        // get RequestDocumentAction model definition
        const model = configuration.getModelDefinition('RequestDocumentAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.findIndex(listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex < 0) {
            // add event listener
            model.eventListeners.push({
                type: listenerType
            });
            // update RequestDocumentAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: EmailDocumentAction service has been successfully installed');
        }
    }

    uninstall() {
        /**
         * get data configuration
         * @type {DataConfigurationStrategy}
         */
        const configuration = this.getConfiguration().getStrategy(DataConfigurationStrategy);
        // get RequestDocumentAction model definition
        const model = configuration.getModelDefinition('RequestDocumentAction');
        // get this file path relative to application execution path
        const listenerType = './' + path.relative(this.getApplication().getConfiguration().getExecutionPath(), __filename);
        // ensure model event listeners
        model.eventListeners = model.eventListeners || [];
        // try to find event listener
        const findIndex = model.eventListeners.find( listener => {
            return listener.type === listenerType ||
                listener.type === listenerType.replace(/\.js$/, '');
        });
        if (findIndex>=0) {
            // remove event listener
            model.eventListeners.splice(findIndex, 1);
            // update RequestDocumentAction model definition
            configuration.setModelDefinition(model);
            // write to log
            TraceUtils.info('Services: EmailDocumentAction service has been successfully uninstalled');
        }
    }
}
