import { DataError, DataNotFoundError } from '@themost/common';
import { DataObjectState } from '@themost/data';
import ActionStatusType from '../models/action-status-type-model';
import { DataConflictError } from '../errors';

/**
 * @async
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
	if (event.state !== DataObjectState.Insert) {
		return;
	}
	const target = event.target;
	const context = event.model.context;
	// validate study program
	if (target.studyProgram == null) {
		throw new DataError(
			'E_STUDY_PROGRAM',
			'The study program cannot be empty at this context',
			null,
			'DeleteProgramAction',
			'studyProgram'
		);
	}
	const studyProgram = await context
		.model('StudyProgram')
		.where('id')
		.equal(target.studyProgram.id || target.studyProgram)
		.select(
			'id',
			'name',
			'department/name as department',
			'studyLevel/name as studyLevel'
		)
		.expand('groups')
		.getItem();
	if (studyProgram == null) {
		throw new DataNotFoundError(
			'The specified study program cannot be found or is inaccessible.'
		);
	}
	// validate that studyProgram does not have any groups
	if (Array.isArray(studyProgram.groups) && studyProgram.groups.length) {
		throw new DataConflictError(
			context.__(
				'The study program cannot be deleted because it contains at least one group.'
			)
		);
	}
	// validate that no students attend it
	const studentsInProgram = await context
		.model('Student')
		.where('studyProgram/id')
		.equal(studyProgram.id)
		.select('id')
		.silent()
		.count();
	if (studentsInProgram) {
		throw new DataConflictError(
			context.__(
				'The study program cannot be deleted because at least one student attends it.'
			)
		);
	}
	// validate that study program does not have any courses
	const specializationCourses = await context
		.model('SpecializationCourse')
		.where('studyProgramCourse/studyProgram/id')
		.equal(studyProgram.id)
		.select('id')
		.silent()
		.count();
	if (specializationCourses) {
		throw new DataConflictError(
			context.__(
				'The study program cannot be deleted because it contains at least one specialization course.'
			)
		);
	}
	// assign a description to the action
	event.target.description = `${context.__('Delete study program')}: [${
		studyProgram.department
	}] - ${studyProgram.name}, ${studyProgram.studyLevel}.`;
}

/**
 * @async
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
	if (event.state !== DataObjectState.Update) {
		return;
	}
	const target = event.model.convert(event.target);
	// get action status
	const actionStatus = await target.property('actionStatus').getItem();
	// if action status is other that active
	if (
		(actionStatus && actionStatus.alternateName) !==
		ActionStatusType.CompletedActionStatus
	) {
		// exit
		return;
	}
	// get previous action status
	const previousActionStatus = event.previous && event.previous.actionStatus;
	if (previousActionStatus == null) {
		throw new DataError(
			'E_PREVIOUS_STATUS',
			'The previous action status cannot be empty at this context.',
			null,
			'DeleteProgramAction'
		);
	}
	// validate previous status
	if (
		previousActionStatus.alternateName !== ActionStatusType.ActiveActionStatus
	) {
		// throw error for invalid previous action status
		throw new DataError(
			'E_STATUS_STATE',
			'Invalid action state. The action cannot be completed due to its previous state.',
			null,
			'UpdateStudentStatusAction'
		);
	}
	// and delete study program
	const context = event.model.context;
	await context.model('StudyProgram').remove({
		id: event.previous.studyProgram,
	});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	// execute async method
	return afterSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
	// execute async method
	return beforeSaveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
