import {EdmMapping,EdmType} from '@themost/data/odata';
import AllocateAction = require('./allocate-action-model');

/**
 * @class
 */
declare class AcceptAction extends AllocateAction {

     
     /**
      * @description Μοναδικός κωδικός
      */
     public id: number; 

}

export = AcceptAction;