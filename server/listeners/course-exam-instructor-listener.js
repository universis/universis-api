import { DataError } from '@themost/common';
import ActionStatusType from '../models/action-status-type-model';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

export function beforeRemove(event, callback) {
	return CourseExamInstructorListener.beforeRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

class CourseExamInstructorListener {
	static async beforeRemoveAsync(event) {
		/**
		 * @type {DataContext|*}
		 */
		const context = event.model.context;
		const courseExamId = event.target.courseExam;
		// get instructor user
		const instructorUser = await context
			.model('Instructor')
			.where('id')
			.equal(event.target.instructor)
			.select('user')
			.getItem();
		if (!(instructorUser && instructorUser.user)) {
			return;
		}
		// check if instructor has previously submitted grades for the specific exam and throw error
		const submittedGrades = await context
			.model('ExamDocumentUploadAction')
            .where('actionStatus/alternateName')
			.equal(ActionStatusType.CompletedActionStatus)
			.or('actionStatus/alternateName')
			.equal(ActionStatusType.ActiveActionStatus)
			.or('actionStatus/alternateName')
			.equal(ActionStatusType.FailedActionStatus)
			.and('object')
			.equal(courseExamId)
			.and('owner')
			.equal(instructorUser.user)
			.count();
        
		if (submittedGrades > 0) {
			throw new DataError(
				'E_COURSE_EXAM_INSTRUCTOR',
				context.__(
					'Course exam instructors with submitted grades cannot be deleted'
				),
				null,
				'CourseExamInstructor'
			);
		}
	}
}
