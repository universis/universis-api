/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */

import { DataError } from "@themost/common";
import { GenericDataConflictError } from "../errors";
import ActionStatusType from '../models/action-status-type-model';

export function beforeSave(event, callback) {
    return StudentProgramGroupEventListener.beforeSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export function afterExecute(event, callback) {
    return StudentProgramGroupEventListener.afterExecuteAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return StudentProgramGroupEventListener.afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

export function beforeRemove(event, callback) {
    return StudentProgramGroupEventListener.beforeRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
    return StudentProgramGroupEventListener.afterRemoveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}

class StudentProgramGroupEventListener {

    static async beforeSaveAsync(event) {
        const student = event.previous ? event.previous.student :
            event.target.student ? (event.target.student.id || event.target.student) : null;
        if (student == null) {
            throw new DataError('E_STUDENT', 'The student may not be empty.', null, 'StudentProgramGroup', 'student');
        }
        const context = event.model.context;
        const studentStatus = await context.model('Student')
            .where('id').equal(student)
            .select('studentStatus/alternateName')
            .silent()
            .value();
        // student must be active
        if (studentStatus !== 'active') {
            throw new GenericDataConflictError('ESTATUS', context.__('Student is not active.'));
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterSaveAsync(event) {

    }

    static async beforeRemoveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.previous == null) {
            throw new Error('The previous state of action cannot be determined.');
        }
        const studentStatus = await context.model('Student')
            .where('id').equal(event.previous.student)
            .select('studentStatus/alternateName')
            .silent()
            .value();
        // student must be active
        if (studentStatus !== 'active') {
            throw new GenericDataConflictError('ESTATUS', context.__('Student is not active.'));
        }
        // Get Student Courses with specific program group - if any
        const studentCourses = await context.model('StudentCourse')
            .where('student').equal(event.previous.student)
            .and('programGroup').equal(event.previous.programGroup)
            .select('count(id) as total').silent().value();

        if (studentCourses && studentCourses > 0) {
            throw new GenericDataConflictError('E_PROGRAMGROUP_CONNECTED', context.__('The student program group cannot be removed because it is connected to a registered student course'), null, 'StudentCourse');
        }
    }

    static async afterRemoveAsync(event) {
        /**
         * @type {DataContext|*}
         */
        const context = event.model.context;
        if (event.previous == null) {
            throw new Error('The previous state of action cannot be determined.');
        }
        // Find student group actions for specific student & programGroup id
        const studentGroupAction = await context.model('UpdateStudentGroupAction')
            .where('object').equal(event.previous.student)
            .and('actionStatus/alternateName').equal('CompletedActionStatus')
            .expand('groups').and('groups/id').equal(event.previous.programGroup)
            .orderByDescending('id')
            .silent().getItem();

        if (studentGroupAction?.groups?.length === 1) {
            // Assign cancelled status
            const action = {
                id: studentGroupAction.id,
                actionStatus: {
                    alternateName: ActionStatusType.CancelledActionStatus,
                }
            };
            // and update
            await context.model('UpdateStudentGroupAction').save(action);
        }
    }

    /**
     * @param {DataEventArgs} event
     */
    static async afterExecuteAsync(event) {

        // add available attachment types related with graduationEvent (only if one record is returned)
        let data = event['result'];
        // validate data
        if (data == null) {
            return;
        }
        // validate query and exit
        if (event.query.$group) {
            return;
        }
        if (!(Array.isArray(data) && data.length)) {
            return;
        }

        const context = event.model.context;
        for (let i = 0; i < data.length; i++) {
            /**
             * @type {StudentProgramGroup|*}
             */
            let studentGroup = context.model('StudentProgramGroup').convert(data[i]);
            if (studentGroup.calculateGroupGrade === 1 && studentGroup.grade == null) {
                studentGroup = await studentGroup.calculate();
                data[i] = studentGroup;
            }

        }
    }
}
