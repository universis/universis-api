import {EdmMapping,EdmType} from '@themost/data/odata';
import {DataObject} from '@themost/data/data-object';
import EnableAttachmentModel from "./enable-attachment-model";
let RequestAction = require('./request-action-model');
/**
 * @class
 
 * @property {Student|any} student
 * @property {Array<StudentMessage|any>} messages
 * @property {number} id
 * @augments {DataObject}
 * @augments {EnableAttachmentModel}
 */
@EdmMapping.entityType('StudentRequestAction')
class StudentRequestAction extends EnableAttachmentModel {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * @returns {Attachment}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('addVirtualAttachment', 'Attachment')
    async addVirtualAttachment(data) {
        const attachment = {
            'attachmentType': data.attachmentType,
            'contentType': 'virtual/octet-stream',
            'published': 0
        };
        const result = await this.context.model('Attachments').save(attachment);
        const requestAttachment = {
            'object': this.getId(),
            'value': result.id
        };
        await this.context.model('StudentRequestActionAttachments').save(requestAttachment);
        return result;
    }
    /**
     * @returns {Attachment}
     */
    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('removeVirtualAttachment', 'Attachment')
    async removeVirtualAttachment(data) {
         // get studentRequestActionAttachments
        const requestAttachment = await this.context.model('StudentRequestActionAttachments')
            .where('object').equal(this.getId())
            .and('value').equal(data.id)
            .getItem();
        if (requestAttachment) {
            // delete attachment
            await this.context.model('StudentRequestActionAttachments').silent().remove(requestAttachment);
            //check if attachment is related to acceptAction
            const acceptAction = await this.context.model('AcceptAttachmentAction').where('object').equal(data.id).getItem();
            if (acceptAction) {
                await this.context.model('AcceptAttachmentAction').silent().remove(acceptAction);
            }
            // finally remove attachment
            await this.context.model('Attachments').silent().remove(data);
        }
        return null;
    }
}
module.exports = StudentRequestAction;
