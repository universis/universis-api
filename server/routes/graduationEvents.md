### Graduation Events Services Summary

The following document contains information about the core services for getting and editing data related to a graduationEvent.
 
Model graduationEvent inherits Event from @universis/events module
 
#### Add graduation event
 
 Add a graduation event for specific department and study programs
 
     POST /api/graduationEvents
     Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
     Content-Type: application/json
     Accept-Language: el
     
 where authorization header contains user access token received from OAuth2 authentication server,
  
  POST data
  
          {
              "organizer": 421,
              "name": "June 2020",
              "graduationYear": 2019,
              "graduationPeriod": 2,
              "studyPrograms": [
                  {
                      "id": 20000003
                  },
                  {
                      "id": 20000110
                  }
              ],
              "validFrom": "2020-03-31T00:00:00.000Z",
              "validThrough": "2020-04-30T00:00:00.000Z"
               ...
          }
     
 The response of this operation is a GraduationEvent object:
 
     {
       "organizer": 421,
       "name": "June 2020",
       "graduationYear": 2019,
       "graduationPeriod": 2,
         "studyPrograms": [
             {
                 "id": 20000003,
                 "$state":4
             },
             {
                 "id": 20000110
             }
         ],
         "validFrom": "2020-03-31T00:00:00.000Z",
         "validThrough": "2020-04-30T00:00:00.000Z",
         "identifier": "5142D28F-1A55-45EF-A4EC-01A6BD6B93A3",
         "eventStatus": {
             "id": 1
         }
        ....
         "id": 1
     }
 
 To remove a studyProgram from a graduationEvent add $state to study program object
 
         "studyPrograms": [
             {
                 "id": 20000003,
                 "$state":4
             },
             {
                 "id": 20000110
             }
         ],

#### Add available attachment types to a graduation event
 
 Add attachment configuration for specific graduation event (object)
 
 
    POST /api/GraduationAttachmentConfigurations
         Authorization: Bearer 34417a537741525365376f76546f5048586761367251454c
         Content-Type: application/json
         Accept-Language: el
         
     where authorization header contains user access token received from OAuth2 authentication server,
      
  POST data 
  
          {
              "attachmentType": 2,
              "object": {
                  "id": 1
              }
          }
 
 The response of this operation is a GraduationAttachmentConfiguration object:
 
      {
         "attachmentType": 2,
         "object": "F7EF0A77-1445-467B-BD49-609D2D48DEBD",
         "target": "GraduationEvent",
         "numberOfAttachments": 1,
         "dateCreated": "2020-04-06T07:54:53.588Z",
         "dateModified": "2020-04-06T07:54:53.801Z",
         "createdBy": 20000506,
         "modifiedBy": 20000506,
         "id": 7
     }
